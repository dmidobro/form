import React from "react";
import { Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { TextField } from "formik-material-ui";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: 10,
  },
  workinghours: {
    height: 50,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
}));
const phoneRegExp = /(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\]?[(]?[0-9]{1,3}[)]?([-\s\]?[0-9]{3})([-\s\]?[0-9]{3,4})/;

const validationSchema = Yup.object().shape({
  title: Yup.string()
    .min(2, "Must be loonger")
    .max(20, "Must be shorter than 20")
    .required("Must enter a title"),
  phone: Yup.string().matches(phoneRegExp, "Phone number is not valid"),
});

export default function FormikForm() {
  const classes = useStyles();
  return (
    <Formik
      initialValues={{ title: "", phone: "", workingHours: "", mon: "" }}
      validationSchema={validationSchema}
      onSubmit={(values, _) => {
        console.log(values);
      }}
    >
      {({ values, errors, touched, handleChange, handleBlur }) => (
        <Form>
          <div>
            <FieldSet size={8} label="Title" name="title" />
          </div>
          <div>
            <FieldSet size={8} label="Phone" name="phone" />
          </div>
          <div>
            <Grid container align="right" alignItems="center">
              <Grid item xs={4}>
                <div className={classes.workinghours}>Working hours:</div>
              </Grid>
              <FieldSet label="Mon" name="mon" />
              <FieldSet label="Tue" name="tue" />
              <FieldSet label="Wed" name="wed" />
              <FieldSet label="Thu" name="thu" />
              <FieldSet label="Fri" name="fri" />
              <FieldSet label="Sat" name="sat" />
              <FieldSet label="Sun" name="sun" />
            </Grid>
          </div>
          <div className={classes.button}>
            <Grid container>
              <Grid item xs={10} align="right">
                <Button type="submit" variant="contained" color="primary">
                  Save
                </Button>
              </Grid>
            </Grid>
          </div>
        </Form>
      )}
    </Formik>
  );
}

const FieldSet = (props) => {
  return (
    <Grid
      container
      alignItems="center"
      spacing={0}
      align="right"
      justify="center"
    >
      <Grid item xs={4}>
        {props.label}
      </Grid>
      <Grid item xs={8}>
        <Field
          style={{ width: "100%", margin: 3 }}
          component={TextField}
          name={props.name}
          type="text"
          variant="outlined"
        />
      </Grid>
    </Grid>
  );
};
